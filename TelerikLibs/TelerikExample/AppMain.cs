﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;

namespace TelerikExample
{
    public static class AppMain
    {
        public static IList<Worker> Workers;
        public static StringBuilder Logs;
        public static Form MainFormInstance;

        private static string logFile = "log.txt";
        private static string dataFile = "db.bin";

        public static void LoadDB( )
        {
            if(!File.Exists(dataFile)) {
                Workers = new List<Worker>();
                CreateData();
                return;
            }

            using(var sr = new FileStream(dataFile, FileMode.Open)) {
                var seri = new BinaryFormatter();
                lock(Workers)
                    Workers = (IList<Worker>)seri.Deserialize(sr);    
            }
        }

        public static void UpdateDB( )
        {
            if(Workers == null)
                return;

            using(var sw = new FileStream(dataFile, FileMode.Open))
            {
                var seri = new BinaryFormatter();
                lock(Workers)
                    seri.Serialize(sw, Workers );
            }
        }

        public static void OnUnloadProject( )
        {
            if(Logs.Length > 0)
                File.WriteAllText(logFile, Logs.ToString());
        }


        private static void CreateData( )
        {
            UpdateDB();           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace TelerikExample
{
    
    [Serializable]
    public class Worker
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public short Age { get; set; }
        public string AccountLink { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }

    }
}

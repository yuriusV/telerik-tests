﻿namespace TelerikExample
{
    partial class UserEditPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            this.datas = new Telerik.WinControls.UI.RadDataEntry();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.images = new System.Windows.Forms.ImageList(this.components);
            this.deleteButton = new Telerik.WinControls.UI.RadButton();
            this.cancelButton = new Telerik.WinControls.UI.RadButton();
            this.updateButton = new Telerik.WinControls.UI.RadButton();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.datas)).BeginInit();
            this.datas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            this.SuspendLayout();
            // 
            // datas
            // 
            this.datas.Location = new System.Drawing.Point(30, 71);
            this.datas.Name = "datas";
            // 
            // datas.PanelContainer
            // 
            this.datas.PanelContainer.Size = new System.Drawing.Size(486, 272);
            this.datas.Size = new System.Drawing.Size(488, 274);
            this.datas.TabIndex = 0;
            this.datas.Text = "radDataEntry1";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Source Sans Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(96, 19);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(230, 46);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Редагування";
            // 
            // images
            // 
            this.images.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.images.ImageSize = new System.Drawing.Size(16, 16);
            this.images.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(293, 362);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(110, 24);
            this.deleteButton.TabIndex = 2;
            this.deleteButton.Text = "Видалити";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(409, 362);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(110, 24);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Відмінити";
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(177, 362);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(110, 24);
            this.updateButton.TabIndex = 4;
            this.updateButton.Text = "Зберегти";
            // 
            // radButton4
            // 
            this.radButton4.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton4.Location = new System.Drawing.Point(31, 19);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(59, 46);
            this.radButton4.TabIndex = 5;
            // 
            // UserEditPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radButton4);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.datas);
            this.Name = "UserEditPage";
            this.Size = new System.Drawing.Size(550, 399);
            ((System.ComponentModel.ISupportInitialize)(this.datas)).EndInit();
            this.datas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadDataEntry datas;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.ImageList images;
        private Telerik.WinControls.UI.RadButton deleteButton;
        private Telerik.WinControls.UI.RadButton cancelButton;
        private Telerik.WinControls.UI.RadButton updateButton;
        private Telerik.WinControls.UI.RadButton radButton4;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TelerikExample
{
    public class StatisticBuilder
    {

        private IList<Worker> workers; 

        public StatisticBuilder( IList<Worker> workers) {
            this.workers = workers;
        }

        public IList<Tuple<short, decimal>> GetAverageSalariesPerAge( ) {
            var result = new List<Tuple<short, decimal>>();
            var ages = new List<short>();
            var averageSalary = new List<decimal>();
            var countSalaries = new List<int>();

            foreach(var user in workers) {
                if(ages.Contains(user.Age))
                {
                    var index = ages.IndexOf(user.Age);
                    averageSalary[index] = (averageSalary[index] * countSalaries[index] + user.Salary) / (countSalaries[index] + 1);
                    countSalaries[index]++;
                }
                else {
                    ages.Add(user.Age);
                    averageSalary.Add(user.Salary);
                    countSalaries.Add(1);
                }
            }

            for(int i = 0; i < ages.Count; i++) {
                result.Add(new Tuple<short, decimal>(ages[i], averageSalary[i]));
            }
             
            return result;
        } 
    }
}
